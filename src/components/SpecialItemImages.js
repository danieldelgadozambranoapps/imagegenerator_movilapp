import React, { useEffect, useState } from 'react'
import { StyleSheet, Modal, TouchableOpacity, Image } from 'react-native'

import ImageViewer from 'react-native-image-zoom-viewer'

const SpecialItemImages = ({urls}) => {
    const [visible, setVisible] = useState(false)

    useEffect(()=>{
    //  console.log("llegando <------------- " )
    },[])
 
  return ( 
    <>
    { urls &&
    <>
      <TouchableOpacity onPress={()=>{setVisible(!visible)}} >  
        <Image source={{uri:urls['0'].url}} style={styles.image} />
      </TouchableOpacity>
      <Modal visible={visible} transparent={true}>
        <ImageViewer enableSwipeDown onSwipeDown={()=>setVisible(!visible)} imageUrls={urls}/>
      </Modal> 
    </>
    }
    </>
  )
}

export default SpecialItemImages;

const styles = StyleSheet.create({
  activityStyle:{
    size:"large",
    color:"#BC00FF"
  },
  image: {
    height: 90,
    width: 90,
    marginLeft:10,
    marginRight:10,
    marginTop:20,
    alignSelf:'center',
    borderRadius:10
  },
  activityStyle:{
    size:"large",
    color:"#BC00FF"
  }
})