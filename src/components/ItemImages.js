import React, { useState, useEffect } from 'react'
import { StyleSheet, Modal, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ImageViewer from 'react-native-image-zoom-viewer'

import { getFullStorageItemPathAlternative, listFilesAndDirectoriesGeneric } from '../functions/firebase-storage-functions'

const ItemImages = ({collection, itemId}) => {
    const [isLoading, setIsLoading] = useState(true)
    const [visible, setVisible] = useState(false)
    const [listData, setListData] = useState(null)
   
    let [imagesArray, setImagesArray] = useState(null)

    useEffect(()=>{
        listFilesAndDirectoriesGeneric("", collection, itemId, setListData)
    },[]) 
 
    useEffect(()=>{ 
      let imagesUrlsArray = []
      if(listData){ 
        let filename 
        for (const subitem of listData){
          console.log(" subitem -------------> " + subitem )
          filename = subitem.path.substring(subitem.path.lastIndexOf('/') ) // + 1 ????
          getFullStorageItemPathAlternative(collection, itemId, filename, imagesUrlsArray, setIsLoading )
        }
      } 
      setImagesArray(imagesUrlsArray)
    },[listData])

  return (
    <>
      { isLoading || !imagesArray || !imagesArray['0'] || !imagesArray['0'].url ?
      <>
        <ActivityIndicator color="#BC00FF" style={styles.activityStyle} />
      </>
        :
      <>
        <TouchableOpacity onPress={()=>{setVisible(!visible)}}>  
          <Image source={{uri:imagesArray['0'].url}} style={styles.image} />
        </TouchableOpacity>
        <Modal visible={visible} transparent={true}>
          <ImageViewer enableSwipeDown onSwipeDown={()=>setVisible(!visible)} imageUrls={imagesArray}/>
        </Modal> 
      </>    
      }
    </>
  )
}

export default ItemImages;

const styles = StyleSheet.create({
  activityStyle:{
    size:"large",
    color:"#BC00FF"
  },
  image:{
    width: '100%',
    height:'100%',
    resizeMode: "contain",
    alignItems: "center",
    borderRadius:5,
    alignSelf:'center'
  
  },
  activityStyle:{
    size:"large",
    marginTop:250
  }
})