import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'

const ProgressBar =({setMarginFix, setDownload, frecuency=3000, message="Loading.....", setDownloading, hasTop=0}) => {
  const [percentage , setPercentage] = useState(0)
  let index = 0

  useEffect(()=>{
    console.log("frecuencia recibida --------> " + frecuency)
    contar()
    if(setMarginFix) setMarginFix(-30)
  },[] )

  const contar =()=>{
    if(index < 98){
      console.log("contando -----------> " + index)
      index = index +1
      setPercentage(index + 1)
      setTimeout(contar , frecuency) 
    } else {
      if(setDownload) setDownload(false)
      if(setDownloading) setDownloading(false)
    }
  }

  return (
    <>
      <View style={styles.container}>
      <View style={{marginTop:hasTop}} />
        <View style={{alignItems:'center', marginBottom:5}} >
          <Text> {message} </Text>
        </View>
        <View style={{ height: 20, flexDirection: "row", width: '90%', backgroundColor: 'white', borderColor: 'gray', borderWidth: 1, borderRadius:10, alignSelf:'center'}}>
          <View style={{width:(percentage + "%"), height:'100%', backgroundColor:'#BC00FF', borderBottomRightRadius:10, borderTopRightRadius:10, borderRadius:10 }} />
        </View>
        <View style={{alignItems:'center', marginTop:5 }} >
        <Text> {percentage}% </Text>
        </View>
      </View>
    </>

  )
}

export default ProgressBar

const styles = StyleSheet.create({
  container:{
    marginTop:10,
    flex:1,
     backgroundColor:"#FAECFF",
      width:'100%',
      height:'90%'
    }
})