import React from 'react'
import { TextInput, StyleSheet } from 'react-native'

const SpecialTextInput = ({search, setData, isEditable }) => {
  return (
    <>
        <TextInput
            style={styles.textInputStyle}
            onChangeText={(text) => {setData(text)} }
            value={search}
            editable={isEditable}
            underlineColorAndroid="transparent"
            placeholder={"What are you thinking ?"}
        />
    </>
  )
}

export default SpecialTextInput

const styles = StyleSheet.create({
    textInputStyle: {
      width:'70%',
      height: 40,
      borderWidth: 1,
      paddingLeft: 20,
      margin: 5,
      borderColor: '#BC00FF',
      backgroundColor: '#F4F4F4',
      alignSelf:'center',
      borderRadius:5,
    }
  })
  
  