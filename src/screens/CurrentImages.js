import React, { useEffect, useState } from 'react'
import { StyleSheet, FlatList, View, TouchableOpacity, ActivityIndicator, LogBox } from 'react-native'
import { BannerAd, TestIds  } from 'react-native-google-mobile-ads'
import Fontisto from 'react-native-vector-icons/Fontisto'

import { getFullStorageItemPathAlternative, listFilesAndDirectoriesGeneric, DeletetePictures } from '../functions/firebase-storage-functions'
import { GetSpecificValueFromAsyncStorage } from '../storage/storage-functions'
import { CheckConnectivity, downloadImage } from "../functions/general-functions"

import SpecialWarningAlert from '../components/SpecialWarningAlert'
import SpecialItemImages from '../components/SpecialItemImages'
import WarningAlert from "../components/WarningAlert"
import ProgressBar from '../components/ProgressBar'
import MyButton from '../components/MyButton'

LogBox.ignoreLogs(['Possible Unhandled Promise Rejection'])

 const CurrentImages = ({navigation}) => {
  const [ warningConnection, setWarningConnection ] = useState(true)
  const [ warningDelete, setWarningDelete ] = useState(false)
  const [ downloading, setDownloading ] = useState(false)
  const [ imagesArray, setImagesArray ] = useState(null)
  const [ dataExists, setDataExists ] = useState(false)
  const [ frecuency, setFrecuency ] = useState(null)
  const [ isLoading, setIsLoading ] = useState(true)
  const [ download, setDownload ] = useState(false)
  const [ listData, setListData ] = useState(null)
  const [ userMail, setUserMail ] = useState(null)
  const [ update, setUpdate ] = useState(false)

  let collection = "Solicitudes_Finalizadas"
  
  const beforeDelete =() => DeletetePictures("", collection, userMail)
  const updateLoading = () => setIsLoading(false)

  useEffect(()=>{
    GetSpecificValueFromAsyncStorage('email', setUserMail)
    if(CheckConnectivity()){
      setWarningConnection(true)
    } else {
      setWarningConnection(false)
    }
  },[])

  useEffect(()=>{
    if(userMail) setDataExists(listFilesAndDirectoriesGeneric("", collection, userMail, setListData))
  },[userMail, update])

  const superDownload = ()=>{
    if(!isLoading) {
      switch(imagesArray.length){
        case 5:
          setFrecuency(100)
        break
        case 10:
          setFrecuency(200)
        break
        case 15:
          setFrecuency(300)
        break
        case 20:
          setFrecuency(400)
        break 
        case 24:
          setFrecuency(400)
        break 
      }
      setDownloading(true)
      if(imagesArray.length > 0 && !isLoading && frecuency){
        for(const url of imagesArray){
          console.log("Descargando esta Foto --->" + url['url'])
          downloadImage(url['url'])
        }
      } 
    }
  }

  useEffect(()=>{ 
    let imagesUrlsArray = []
    if(listData && dataExists && warningConnection){  
      setTimeout(updateLoading , 3000) 
      let filename 
      for (const subitem of listData){
        filename = subitem.path.substring(subitem.path.lastIndexOf('/') ) // + 1 ????
        getFullStorageItemPathAlternative(collection, userMail, filename, imagesUrlsArray)
      }
    } 
    setImagesArray(imagesUrlsArray)
    },[listData])

  const render = ({item}) => { 
    let localArray = []
        localArray.push({url:item['url']})
      return <SpecialItemImages urls={localArray} />
  }

  return (
    <>
      <View style={styles.container}>
        { !downloading ?
          <>
            <View style={styles.box} > 
              { warningConnection && imagesArray && !isLoading?
                <FlatList data={imagesArray} renderItem={(item) => render(item)} numColumns={3} keyExtractor={(item, index) => index.toString()} />
              :
              <> 
                { !isLoading && !imagesArray ?
                  <ActivityIndicator color="#BC00FF" style={styles.activityStyle} />
                :
                  <ActivityIndicator color="#BC00FF" style={styles.activityStyle} />
                }
              </>
              }
              </View>
            <SpecialWarningAlert status={warningDelete} cancelButton={true} setStatus={setWarningDelete} description={"Are you sure you want to delete the current images ? "} runFunction={beforeDelete} setUpdate={setUpdate} update={update} />
            <WarningAlert status={!warningConnection} setStatus={setWarningConnection} description={"There is no conection, try again later ."} />
            <WarningAlert status={download} setStatus={setDownload} description={"All the photos are now in your photo library ."} mainTitle={"Download Successful ! "} />
            <TouchableOpacity style={styles.button} onPress={()=>{setWarningDelete(true)} } > 
              <Fontisto name={'trash'} size={30} color={'#ffffff'} />
            </TouchableOpacity>

            <View style={{marginTop:10}} >
              <BannerAd 
                  size={"350x60"} 
                  // size={BannerAdSize.BANNER}  // --> dafault 320x50
                  unitId={TestIds.BANNER} 
                  // unitId={"ca-app-pub-2584779830071009/6515980228"} 
                  // requestOptions={{
                  //   requestNonPersonalizedAdsOnly:true,
                  // }}
                />
            </View>
       
            <MyButton title={"Download"} customClick={()=>{superDownload()}} />

        </>
        :
        <>
          <ProgressBar frecuency={frecuency} setDownload={setDownload}  hasTop={"65%"}  downloading="Downloading ..." setDownloading={setDownloading} message={"Dowloading ..."}/>
        </>
        }
      </View>
    </>
  )
}
export default CurrentImages

const styles = StyleSheet.create({
  container:{
    flex:1,
     backgroundColor:"#FAECFF",
     alignItems:'center'
  },
  box:{
    width:'85%',
    height:'75%',
    backgroundColor:'#F3CFFF',
    alignSelf:'center',
    borderRadius:10,
    marginTop:30,
    alignItems:'center',
    marginBottom:10,
    elevation:10
  },
  image: {
    height: 90,
    width: 90,
    marginLeft:10,
    marginRight:10,
    marginTop:20,
    alignSelf:'center',
    borderRadius:10
  },
  button: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#BC00FF',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 160,
    right: 13,
    elevation: 5,
  },
  activityStyle:{
    size:"large",
    marginTop:250
  }
})