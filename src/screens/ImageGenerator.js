import React, { useState, useEffect } from 'react'
import { View, Image, TouchableOpacity, LogBox, Button, Text, ActivityIndicator, StyleSheet } from 'react-native'

import { sendImageRequest, checkDocument, isDatabaseAvailable, checkIfIsGenerate } from "../functions/firebase-firestore-funtions"
import { BannerAd, TestIds, InterstitialAd, AdEventType  } from 'react-native-google-mobile-ads'
import { DownloadAllPictures, DeletetePictures } from '../functions/firebase-storage-functions'
import { GetSpecificValueFromAsyncStorage } from "../storage/storage-functions"
import { CheckConnectivity, checkWords } from "../functions/general-functions"

import SpecialTextInput from '../components/SpecialTextInput'
import SpecialPicker from '../components/SpecialPicker'
import WarningAlert from "../components/WarningAlert"
import ProgressBar from '../components/ProgressBar'
import ItemImages from "../components/ItemImages"
import MyButton from '../components/MyButton'

LogBox.ignoreLogs(['Possible Unhandled Promise Rejection'])
LogBox.ignoreLogs(['Require'])

const adUnitId = __DEV__ ? TestIds.INTERSTITIAL : 'ca-app-pub-xxxxxxxxxxxxx/yyyyyyyyyyyyyy';

const interstitial = InterstitialAd.createForAdRequest(adUnitId, {
  requestNonPersonalizedAdsOnly: true,
 // keywords: ['fashion', 'clothing'],
});

const ImageGenerator = ()=> {
  const [ warningDescription, setWarningDescription ] = useState(false)
  const [ warningConnection, setWarningConnection ] = useState(false)
  const [ downloadFrecuency, setdownloadFrecuency ] = useState(100)
  const [ currentInLine , setCurrentInLine] = useState(false)
  const [ warningServer, setWarningServer ] = useState(false)
  const [ loadingLoopAd, setLoadingLoopAd ] = useState(true)
  const [ triggerWords, setTriggerWords ] = useState(false)
  const [ amountPictures, setAmountPictures ] = useState(5)
  const [ warningTime, setWarningTime ] = useState(false)
  const [ downloading, setDownloading ] = useState(false)
  const [ requestSend, setRequestSend ] = useState(false)
  const [ adMobUpdate, setAdMobUpdate ] = useState(false)
  const [ firstCheck, setFirstCheck ] = useState(300000)
  const [ showButton, setShowButton ] = useState(false)
  const [ isEditable, setIsEditable ] = useState(true)
  const [ frecuency, setFrecuency ] = useState(300000)
  const [ download, setDownload ] = useState(false)
  const [ marginFix, setMarginFix ] = useState(90)
  const [ userMail, setUserMail ] = useState(null)
  const [ loading, setLoading ] = useState(false)
  const [ search, setSearch ] = useState(null)

  let frecuency_retry = 10000

  useEffect(()=>{
    GetSpecificValueFromAsyncStorage('email', setUserMail)
  },[])

  const checkStorage = async ()=>{
    setLoadingLoopAd(false)
    if(await checkDocument(userMail, setLoading, setSearch, setShowButton)){
      setTimeout(checkStorage , frecuency_retry) 
    } else {
      setLoadingLoopAd(false)
      setIsEditable(true)
    }
  }

  const checkIsGenerated = async () => {
    if(await checkIfIsGenerate(userMail) == false){
      console.log(" checkeando si se comenzo a generar")
      setTimeout(checkIsGenerated , 15000) 
    } else {
      setRequestSend(true)
      setLoading(true)
      DeletetePictures("", "Solicitudes_Finalizadas", userMail)
      setCurrentInLine(false)
      cargarPublicidad()
      setTimeout(checkStorage, firstCheck)
    }
  }

  const checkBeforeSend = async () => {
    if(CheckConnectivity()){
      const DatabaseAvailable = await isDatabaseAvailable()
      if(DatabaseAvailable){
        if(search && userMail) {
          if(!checkWords(search)) {
            switch(amountPictures){
              case 5:
                setFrecuency(3000)
                setFirstCheck(300000)
                setdownloadFrecuency(100)
              break
              case 10:
                setFrecuency(4200)
                setFirstCheck(420000)
                setdownloadFrecuency(200)
              break
              case 15:
                setFrecuency(5400)
                setFirstCheck(540000)
                setdownloadFrecuency(300)
              break
              case 20:
                setFrecuency(6600)
                setFirstCheck(660000)
                setdownloadFrecuency(400)
              break 
            }
            setIsEditable(false)
            sendImageRequest(userMail, search, amountPictures) 
            setSearch("We are processing your picture ...")
            setCurrentInLine(true)
            checkIsGenerated()
          } else{
            setTriggerWords(true)
          }
        } else {
          setWarningDescription(true) 
        }
      } else {
        setWarningServer(true) 
      }
    } else {
      setWarningConnection(true)
    }
  } 

  const cargarPublicidad = ()=>{ 
      interstitial.addAdEventListener(AdEventType.LOADED, () => {
      interstitial.show();
    });
    interstitial.load();
  //  interstitial.show()
    setAdMobUpdate(!adMobUpdate)
    if(loadingLoopAd)setTimeout(cargarPublicidad, 60000) 
  }
  
  return (
    <>
      <View style={styles.container} >
        <View style={{flexDirection:'row', marginTop:0}} >
          <SpecialTextInput status={warningDescription} isEditable={isEditable} search={search} setData={setSearch} /> 
            <TouchableOpacity style={styles.cameraImageStyle} onPress={()=>{checkBeforeSend()} }>
              <Image style={styles.LogoImage} source={require('../../assets/imagegeneratorLogo.png')} /> 
            </TouchableOpacity>
            <WarningAlert status={warningDescription}  setStatus={setWarningDescription} description={"You must write a description ..."} />
            <WarningAlert status={warningConnection} setStatus={setWarningConnection} description={"There is no conection, try again later ."} />
            <WarningAlert status={warningServer} setStatus={setWarningServer} description={"Our servers are not available at the moment, please try again later ."} />
            <WarningAlert status={triggerWords} setStatus={setTriggerWords} description={"We dont allow genereate this kind of images !"} />
            <WarningAlert status={download} setStatus={setDownload} description={"All the photos are now in your photo library ."} mainTitle={"Download Successful ! "} />
            <WarningAlert status={warningTime} setStatus={setWarningTime} description={"Remember that a larger number of images will require more download time ."} mainTitle={"Alert !"} />
        </View>
            <View style={{width:'90%', height:335,   backgroundColor:'white', borderRadius:10, marginTop:10, elevation:5 }} >
              <View style={styles.noImageView} >
                { requestSend &&
                <> 
                  { loading ? 
                  <> 
                    <View style={{alignItems:'center', marginTop:"25%"}} >
                      <Image source={require("../../assets/animation.gif")} style={styles.image}/>  
                    </View>
                  </>  
                  :  
                  <> 
                    <View style={styles.noImageView} >
                      <ItemImages collection="Solicitudes_Finalizadas" itemId={userMail} />
                    </View>
                  </>
                  }
                </>
                }
                </View> 
              </View>
            <View style={styles.subBoxStyle} >
            { !loading &&
              <>
                <View style={{flexDirection:"row"}} >
                  { showButton &&
                    <MyButton title={"Download"} customClick={()=>{ DownloadAllPictures("Solicitudes_Finalizadas", userMail, "", setDownload, setDownloading) }} />
                  } 
                  <SpecialPicker pictures={amountPictures} setPictures={setAmountPictures} setWarningTime={setWarningTime}  /> 
                </View>
              </>
            }
            </View>

            { currentInLine &&
            <>
              <View style={{width:'90%'}} > 
                <Text style={styles.preDownloadText} > Our servers will take your request as soon as we can ...  this may take a few seconds </Text>
              </View>
              <ActivityIndicator color="#BC00FF" style={styles.activityIndicator} />
            </>
            }

            { requestSend && loading &&
              <ProgressBar frecuency={frecuency} setMarginFix={setMarginFix} />
            }
            { downloading &&
               <ProgressBar frecuency={downloadFrecuency} setDownload={setDownload}  hasTop={"65%"}  downloading="Downloading ..." setDownloading={setDownloading} message={"Dowloading ..."}/>
            }

            { false &&
              <Button title="Show Interstitial" onPress={() => { cargarPublicidad()}} />
            }
            
            <View style={{top:marginFix}} >
              <BannerAd 
                size={"350x60"} 
                // size={BannerAdSize.BANNER}  // --> dafault 320x50
                unitId={TestIds.BANNER} 
                // unitId={"ca-app-pub-2584779830071009/6515980228"} 
                // requestOptions={{
                //   requestNonPersonalizedAdsOnly:true,
                // }}
                />
            </View>  
        </View>
    </>
  )
}

export default ImageGenerator

const styles = StyleSheet.create({
  container:{
    flex:1,
     backgroundColor:"#FAECFF",
     alignItems:'center'
  },
  cameraImageStyle: {
    width: '20%',
    height: 100,
    borderRadius: 30,
    alignItems: 'center',
  },
  LogoImage:{
    width: "100%",
    height: "100%",
    resizeMode: "contain",
  },
  text:{
    fontSize:17
  },
  image:{
    width: "80%",
    height: 150,
    resizeMode: "contain",
  },
  activityIndicator: {
    alignItems: "center",
    height: 80,
  },
  noImageView:{
    width:"100%", 
    height:"100%", 
    borderRadius:5, 
    borderWidth: 1, 
    borderColor:'gray', 
    alignSelf:'center', 
    backgroundColor:'#F4F4F4', 
    right:0, 
    borderStyle:'dashed', 
    elevation:5
  },
  subBoxStyle:{
    width:'100%',
    alignItems:'flex-end',
    right:5,
    marginTop:5
  },
  preDownloadText:{
    fontSize:14,
    fontStyle:'italic',
    alignSelf:'flex-start',
    marginBottom:5
  }
})