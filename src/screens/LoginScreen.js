import React, { useState } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native'

import { GetAllPermissions} from "../functions/permission-functions"
import { SaveInStorage } from '../storage/storage-functions'

import AwesomeAlert from 'react-native-awesome-alerts'
import FormInput from '../components/FormInput.js'

const LoginScreen = ({ navigation }) => {
    const [email, setEmail] = useState("")
    const [show, setShow] = useState(false)
    const [show2, setShow2] = useState(false)

    const checkMailFormat =(mail)=> {
      var value = mail.includes("@")
      return value
    }

    const temporalfunction = ()=>{
      if(email){
        if(checkMailFormat(email)){
          SaveInStorage('email', email)
          GetAllPermissions()
          navigation.navigate('DrawerNavigator')
          } else {
            setShow2(true)
          }
        } else {
          setShow(true)
        } 
    }
    
    return(
      <View style={styles.container} >
        <ScrollView>
          <Image source={require("../../assets/imagegeneratorLogo.png")} style={styles.image}/>
          <View style={{marginTop:20}} />

          <FormInput
            labelValue={email}
            onChangeText={(userEmail) => setEmail(userEmail)}
            placeholderText="Email"
            iconType="mail"
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
          />

          <AwesomeAlert
            show={show}
            showProgress={false}
            title="Error ..."
            message="Complete your mail and select your region ."
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={false}
            showConfirmButton={true}
            cancelText="No, cancel"
            confirmText="Confirm"
            confirmButtonColor="#BC00FF"
            onConfirmPressed={() => {
            console.log("confirm presed")
            setShow(false)
          }}
          />
          
          <AwesomeAlert
            show={show2}
            showProgress={false}
            title="Error ..."
            message="Please write a valid mail."
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={false}
            showConfirmButton={true}
            cancelText="No, cancel"
            confirmText="Confirm"
            confirmButtonColor="#BC00FF"
            onConfirmPressed={() => {
            console.log("confirm presed")
            setShow2(false)
          }}
          />
          <Text style={styles.subInfoText}> We will not send you any kind of mail, spam or advertising .</Text>
    
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={()=>temporalfunction()}>
            <Text style={styles.buttonTextStyle}> Continuar </Text>
          </TouchableOpacity>
          
        </ScrollView>
      </View>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
container:{
    flex:1,
    backgroundColor:"#FAECFF"
},
text:{
    fontSize:20,
    justifyContent:'center',
    alignItems:'center',
    alignContent:'center',
    alignSelf:'center'
},
image:{
    width: "80%",
    height: 300,
    resizeMode: "contain",
    margin: 30,
    alignItems: "center",
    marginTop:60,
    marginBottom:20
  },
  buttonStyle: {
    backgroundColor: "#BC00FF",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#BC00FF",
    height: 42, 
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 170,
    marginBottom: 25,
    elevation:10
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 17.5,
    fontWeight:'600',
  },
  registerTextStyle: {
    color: "#BC00FF",
    textAlign:'center',
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
    marginLeft:160,
    marginTop:5
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  subInfoText:{
    fontSize:14,
    fontStyle:'italic',
    alignSelf:'flex-start',
    left:15,
    marginTop:0
  }
})

