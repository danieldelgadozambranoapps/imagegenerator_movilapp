import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'

import DrawerNavigator from '../navigators/DrawerNavigator'
import RegisterScreen from '../screens/RegisterScreen'
import SplashScreen from '../screens/SplashScreen'
import LoginScreen from '../screens/LoginScreen'

const Stack = createStackNavigator();

const MainNavigator = ({navigation}) => {

  return(
    <>
        <NavigationContainer>
          <Stack.Navigator
            initialRouteName='SplashScreen'
            screenOptions={({route}) =>({
              headerTintColor:'white',
              activeTintColor: '#BC00FF',
              headerTitleAlign:'center',
              headerStyle:{backgroundColor:'#BC00FF'},
              drawerStyle:{width:220},
              headerTitleStyle:{fontSize:20},
            // drawerLabel:route.name
            headerShown:true
            })}>
            <Stack.Screen name='LoginScreen' component={LoginScreen} options={{ headerShown:false }} />   
            <Stack.Screen name='RegisterScreen' component={RegisterScreen} options={{ headerShown:false }} />  
            <Stack.Screen name='SplashScreen' component={SplashScreen} options={{ headerShown:false }} />   
            <Stack.Screen name='DrawerNavigator' component={DrawerNavigator}  options={{ headerShown:false }}/>
        </Stack.Navigator>
      </NavigationContainer> 
    </>
  )}

export default MainNavigator;