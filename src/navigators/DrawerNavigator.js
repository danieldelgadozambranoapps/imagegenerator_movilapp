import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import ImageGenerator from '../screens/ImageGenerator'
import AboutUs from '../screens/AboutUs'

import CurrentImages from '../screens/CurrentImages'
import CustomSidebarMenu from '../../CustomSidebarMenu'
const Drawer = createDrawerNavigator();

//testeo de login 

const  DrawerNavigator = () => {
  return (
    <Drawer.Navigator
    initialRouteName="Inventario"
    drawerContent={(props) => <CustomSidebarMenu {...props} />}
      screenOptions={({route}) =>({
        headerTintColor:'white',
        activeTintColor: '#BC00FF',
        headerTitleAlign:'center',
        headerStyle:{backgroundColor:'#BC00FF'},
        drawerStyle:{width:220},
        headerTitleStyle:{fontSize:20},
        drawerLabel:route.name ,

      })}>
      <Drawer.Screen name="ImageGenerator" component={ImageGenerator} options={{title :"Image Generator", drawerLabel :"Image Generator", groupName: 'Image Generator', unmountOnBlur:false}}  />
      <Drawer.Screen name="CurrentImages" component={CurrentImages} options={{title :"Recent Images", drawerLabel :"Recent Images", groupName: 'Image Generator', unmountOnBlur:true}}  />
      <Drawer.Screen name="AboutUs" component={AboutUs} options={{title :"About Us", drawerLabel:"About Us", groupName: 'Image Generator', unmountOnBlur:false}}  />
    </Drawer.Navigator>
  )
}

export default DrawerNavigator
